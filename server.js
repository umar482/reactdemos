//  OpenShift sample Node application
var express = require('express'),
    app = express(),
    morgan = require('morgan');

var fs = require('fs')
var key = fs.readFileSync('./server.key')
var cert = fs.readFileSync('./server.crt')
var https = require('https')
var https_options = {
    key: key,
    cert: cert
};

var bodyParser = require('body-parser');
app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true }));

Object.assign = require('object-assign')

app.engine('html', require('ejs').renderFile);
app.use(morgan('combined'))

var port = process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 8080,
    ip = process.env.IP || process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0'

// for localhost ajax
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.use(express.static('public'))

app.get('/', (req, res) => {
    res.sendFile('public/index.html', { root: __dirname });
})


app.get('/guestbook', (req, res) => {
    res.sendFile('public/index.html', { root: __dirname });
})


app.get('/contacts', (req, res) => {
    res.sendFile('public/index.html', { root: __dirname });
})


app.get('/todo', (req, res) => {
    res.sendFile('public/index.html', { root: __dirname });
})


// Guestbook API

var Datastore = require('nedb')
    , db = new Datastore({ filename: 'data/database.db' });

// insert 
app.post('/guestbookapi/insert', (req, res) => {
    var name = req.body.name
    var msg = req.body.msg
    var doc = { name: name, msg: msg, date: new Date() };

    db.loadDatabase(function (err) {
        db.insert(doc, function (err, newDoc) {
            if (err) {
                res.status(400).send(err)
            } else {
                res.status(201).json(newDoc)
            }
        });
    });
})

// select all
app.get('/guestbookapi/all', (req, res) => {
    db.loadDatabase(function (err) {
        db.find({}, function (err, docs) {
            if (err) {
                res.status(400).send(err)
            } else {
                res.status(200).json(docs);
            }
        });
    })

})

app.get('/guestbookapi/last10', (req, res) => {
    db.loadDatabase(function (err) {
        db.find({}).sort({ date: -1 }).limit(10).exec(function (err, docs) {
            if (err) {
                res.status(400).send(err)
            } else {
                res.status(200).json(docs);
            }
        });
    });
})

app.post('/guestbookapi/update', (req, res) => {
    var id = req.body.id
    var name = req.body.name
    var msg = req.body.msg

    db.loadDatabase(function (err) {
        db.update({ _id: id }, { $set: { name: name, msg: msg } }, {}, function (err, numReplaced) {
            if (err) {
                res.status(400).send(err)
            } else {
                res.status(200)
                res.end()
            }
        });
    })
})

app.get('/guestbookapi/remove/:id', (req, res) => {
    var id = req.params.id

    db.loadDatabase(function (err) {
        db.remove({ _id: id }, {}, function (err, numRemoved) {
            if (err) {
                res.status(400).send(err)
            } else {
                res.status(200)
                res.end()
            }
        });
    });
})


// error handling
app.use(function (err, req, res, next) {
    console.error(err.stack);
    res.status(500).send('Something bad happened!');
});



app.listen(port, ip);

// server = https.createServer(https_options, app).listen(port, ip);

// https.createServer(https_options, function (req, res) {
//     res.writeHead(200);
//     res.end("Hi from HTTPS");
// }).listen(8000);

console.log('Server running on https://%s:%s', ip, port);

module.exports = app;
